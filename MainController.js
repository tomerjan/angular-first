(function() {
    var module = angular.module("githubViewer");
    var MainCtrl = function($scope, $interval, $location) {
        var decrementCountDown = function() {
            $scope.countDown -= 1;
            if ($scope.countDown < 1) {
                $scope.search($scope.username);
            }
        };

        var countdownInterval = null;
        var startCountdown = function() {
            countdownInterval = $interval(decrementCountDown, 1000, $scope.countDown);
        };

        $scope.search = function(username) {
            if (countdownInterval) {
                $interval.cancel(countdownInterval);
                $scope.countDown = null;
            }
            $location.path("/user/" + username);
        };

        $scope.username = "angular";
        $scope.countDown = 5;
        startCountdown();
    };

    module.controller("MainCtrl", MainCtrl);
})();
